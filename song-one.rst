.. _song-one::

Song One
########

.. list-table:: 

   * - Det er norsk
     - This is English
   * - Og en linje til
     - And another line
     
     
.. csv-table:: 

   "Det er norsk", "This is English"
   "Og en linje til", "And another line"